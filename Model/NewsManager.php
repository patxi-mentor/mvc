<?php

namespace App\Model;

use PDO;

class NewsManager extends ParentModel
{
    public function getNews(): array
    {
        $query = $this->pdo->query('SELECT * FROM news');

        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getNew(int $id): array
    {
        $actu = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

        return [
            'content' => $actu
        ];
    }

    public function modifyNew(array $new): bool
    {
        return true;
    }

    public function deleteNew(int $id): bool
    {
        return true;
    }

    public function addNew(array $new): array
    {
        return [];
    }
}