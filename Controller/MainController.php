<?php

namespace App\Controller;

use App\Model\NewsManager;

class MainController
{
    public static function home()
    {
        $title = "Page d'accueil qui vient du controller";

        render('home', [
            'title' => $title
        ]);
    }

    public static function actu()
    {
        $manager = new NewsManager();
        //Je récupère l'actu grace au model donc depuis une base de données
        $news = $manager->getNews();

        render('actu', ['news' => $news]);
    }

    public static function about()
    {
        $name = '@patxi';

        render('about', ['name' => $name]);
    }
}