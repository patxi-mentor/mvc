<?php

namespace App\Service;

class Router
{
    public static function run()
    {
        $page = $_GET['page'] ?? 'main::home';

        if (!preg_match('#^(\w+)::(\w+)$#', $page, $matches)) {
            echo "La page $page est incorrecte";
            die;
        }

        [$page, $controller, $method] = $matches;

        $controller = '\App\Controller\\'.ucfirst($controller).'Controller';

        if (method_exists($controller, $method)) {
            $controller::$method();
        } else {
            echo "La page $page n'existe pas";
        }
    }
}