<?php

use JetBrains\PhpStorm\NoReturn;

//Autoloader
spl_autoload_register(function ($className) {
    $fileName = preg_replace('#^App#', __DIR__, $className);
    $fileName .= '.php';
    $fileName = str_replace('\\', '/', $fileName);
    if (file_exists($fileName)) {
        require_once $fileName;
    }
});

function dump(...$vars): void
{
    foreach ($vars as $var) {
        $var = is_string($var) ? htmlspecialchars($var) : $var;
        echo '<pre style="background-color: #333; color: white; padding: 4px; margin: 8px 0">';
        var_dump($var);
        echo '</pre>';
    }
}

#[NoReturn] function dd(...$vars): void
{
    dump(...$vars);
    die;
}

#[NoReturn] function render(string $view, array $viewVars = []): void
{
    foreach ($viewVars as $varName => $value) {
        $$varName = $value;
    }

    ob_start();
    require_once __DIR__.'/view/'.$view.'.phtml';
    $content = ob_get_clean();
    require_once __DIR__.'/view/layout.phtml';
    die;
}